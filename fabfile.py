# -*- coding: utf-8 -*-
# do ssh-add <public key> first to run this script

from fabric.api import cd, env, local, run as remote, sudo, task

env.hosts = ('cyluss.kr',)
env.user = 'ubuntu'


def do_in_venv(action, cmd):
    return action("source bin/activate && " + cmd)


@task
def syncdb():
    do_in_venv(local, 'python manage.py reset --noinput core')
    do_in_venv(local, 'python manage.py syncdb')


@task
def run():
    return do_in_venv(local, 'python manage.py runserver 0.0.0.0:8000')


@task
def watch():
    local("pushd fasticker/core/static && scss --watch app.scss:app.css")


@task
def down():
    sudo('service fasticker stop')
    sudo('service nginx stop')


@task
def up():
    sudo('service fasticker start')
    sudo('service nginx start')


@task
def deploy():
    with cd('fasticker'):
        down()
        remote('git pull')
        do_in_venv(remote, 'python manage.py collectstatic --noinput')
        up()
