#!/bin/bash

PYTHON_PATH=$(which python2.7)

virtualenv -p $PYTHON_PATH .
source bin/activate
pip install -r requirements.txt

project_name=`basename $PWD`
sh syncdb.sh
source bin/activate
