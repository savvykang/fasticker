from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from django.db import models

from fasticker.core.middleware import register_admin
from fasticker.util import media_upload_to


@register_admin
class Color(models.Model):
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name


@register_admin
class Context(models.Model):
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name


@register_admin
class Category(models.Model):
    name = models.CharField(max_length=30)
    parent = models.ForeignKey('self', null=True, blank=True)

    def __unicode__(self):
        if self.parent:
            return u'{} - {}'.format(self.parent, self.name)
        else:
            return self.name


@register_admin
class Preference(models.Model):
    category = models.ForeignKey(Category, null=True, blank=True)
    context = models.ForeignKey(Context, null=True, blank=True)
    color = models.ForeignKey(Color, null=True, blank=True)

    def __unicode__(self):
        return u' '.join(unicode(x) for x in (self.category, self.context, self.color) if x is not None)

    def clean(self):
        super(Preference, self).clean()
        if not any((self.category, self.context, self.color)):
            raise ValidationError('pick at least one condition')


@register_admin
class Item(models.Model):
    author = models.ForeignKey('Profile')
    name = models.CharField(max_length=60)
    memo = models.TextField(max_length=2000)
    image = models.ImageField(upload_to=media_upload_to)
    link = models.URLField(blank=True)

    category = models.ForeignKey(Category)
    context = models.ManyToManyField(Context, null=True, blank=True)
    color = models.ForeignKey(Color, null=True, blank=True)

    tags = models.CharField(max_length=80, blank=True)

    class Meta:
        ordering = ("-id",)

    def __unicode__(self):
        return self.name

    def get_tags(self):
        if self.tags:
            return (tag.strip() for tag in self.tags.split(','))
        else:
            return []


class Linkable(models.Model):
    LINK_ITEM = 'item'
    LINK_EXTERNAL_PAGE = 'external_page'
    LINK_YOUTUBE = 'youtube'

    LINK_CHOICES = (
        (LINK_ITEM, "Item"),
        (LINK_EXTERNAL_PAGE, "Extenal Page"),
        (LINK_YOUTUBE, "Youtube Video")
    )

    link = models.URLField(blank=True)
    link_type = models.CharField(max_length=12,
                                 choices=LINK_CHOICES,
                                 blank=True)

    class Meta:
        abstract = True


@register_admin
class ItemComment(Linkable):
    item = models.ForeignKey("Item")
    author = models.ForeignKey("Profile")
    memo = models.TextField(max_length=300)

    def __unicode__(self):
        return unicode(self.memo)


@register_admin
class Profile(models.Model):
    user = models.OneToOneField(User)
    name = models.CharField(max_length=20)
    photo = models.ImageField(upload_to=media_upload_to, blank=True)
    memo = models.TextField(max_length=100)
    following = models.ManyToManyField('self', blank=True)
    preferences = models.ManyToManyField(Preference, blank=True)

    def __unicode__(self):
        return unicode(self.user)


@register_admin
class Board(models.Model):
    name = models.CharField(max_length=60)
    author = models.ForeignKey(Profile)
    items = models.ManyToManyField(Item, through="ItemBoardContainsRelation")

    def __unicode__(self):
        return u"{} by {}".format(self.name, self.author)


@register_admin
class ItemBoardContainsRelation(models.Model):
    board = models.ForeignKey(Board)
    item = models.ForeignKey(Item)

    class Meta:
        ordering = ('pk',)

    def __unicode__(self):
        return u'{} - {}'.format(self.board.author, self.item)


def recommendations(preferences):
    for p in preferences:
        qs = Item.objects
        if p.category:
            qs = qs.filter(category=p.category)
        if p.color:
            qs = qs.filter(color=p.color)
        if p.context:
            qs = qs.filter(context=p.context)
        for item in qs:
            yield item
