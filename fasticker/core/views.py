from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from fasticker.core import models, forms
from fasticker.core.middleware import require_http_methods
from fasticker.util import autorotate, unique_everseen


@require_http_methods('GET')
def index(request):
    context = {}
    context["items"] = models.Item.objects.all()[:20]
    return render(request, 'index.html', context)


@require_http_methods('GET')
def items(request):
    category_id = request.GET.get("category_id", None)
    if category_id:
        c = models.Category.objects.get(id=category_id)
        ctx = dict(items=c.item_set.all())
    else:
        ctx = dict(items=models.Item.objects.all())

    color_id = request.GET.get("color_id", None)
    if color_id:
        ctx["items"] = ctx["items"].filter(color=color_id)

    context_id = request.GET.get("context_id", None)
    if context_id:
        ctx["items"] = ctx["items"].filter(context__in=context_id)
    ctx["categories"] = models.Category.objects.filter(parent=None)
    ctx["contexts"] = models.Context.objects.all()
    ctx["colors"] = models.Color.objects.all()

    return render(request, 'items.html', ctx)


@require_http_methods('GET')
def recommendations(request):
    profile = models.Profile.objects.get(id=1)
    preferences = profile.preferences.all()

    return render(request,
                  'recommendations.html',
                  dict(recommendations=unique_everseen(models.recommendations(preferences)),
                       preferences=preferences))


@require_http_methods('GET')
def board(request, board_id):
    ctx = dict(board=models.Board.objects.get(id=board_id))
    return render(request, 'board.html', ctx)


@require_http_methods('GET')
def board_form(request, board_id=None):
    ctx = dict(board=models.Board.objects.get(id=board_id))
    return render(request, 'board.html', ctx)


@require_http_methods('GET')
def item(request, item_id):
    ctx = dict(item=models.Item.objects.get(id=item_id))
    return render(request, 'item.html', ctx)


@login_required
@require_http_methods('POST')
def item_comment_form(request, item_id):
    form = forms.ItemCommentForm(request.POST)
    if form.is_valid():
        comment = form.save(commit=False)
        comment.item = models.Item.objects.get(id=item_id)
        comment.author = request.user.profile
        comment.save()
    else:
        print "fuck"
    return redirect(reverse('fasticker.core.views.item', args=(item_id, )))


@login_required
@require_http_methods('GET', 'POST')
def item_form(request, item_id=None):
    if item_id is not None:
        item = models.Item.objects.get(id=item_id)
    if request.method == 'POST':
        if item_id is None:
            form = forms.ItemForm(request.POST, request.FILES)
        else:
            form = forms.ItemForm(request.POST, request.FILES, instance=item)

        if form.is_valid():
            if item_id is None:
                item = form.save(commit=False)
                item.author = request.user.profile
                item.save()
                autorotate(item.image.file.name)
            else:
                form.save()

            return redirect('fasticker.core.views.items')
    else:
        if item_id is None:
            form = forms.ItemForm()
        else:
            form = forms.ItemForm(instance=item)

    ctx = {
        'form': form,
    }
    if item_id is not None:
        ctx["item_id"] = item_id
    return render(request, 'item_form.html', ctx)


@require_http_methods('GET', 'POST')
def join(request):
    if request.method == "POST":
        form = forms.JoinForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request, 'registration/complete.html')
    else:
        form = forms.JoinForm()
    ctx = {"form": form}
    return render(request, 'registration/registration_form.html', ctx)


@require_http_methods('GET')
def profile(request, profile_id):
    profile = models.Profile.objects.get(id=profile_id)
    preferences = profile.preferences.all()
    context = dict(profile=profile,
                   recommendations=unique_everseen(models.recommendations(preferences)),
                   preferences=preferences,
                   boards=models.Board.objects.filter(author=profile_id))

    return render(request, 'profile.html', context)
