from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.utils.translation import ugettext_lazy as _
from fasticker.core import models


class ItemForm(forms.ModelForm):
    class Meta:
        model = models.Item
        fields = ("name", "memo", "image", "category", "context", "color", "link", "tags")

    def clean(self):
        cleaned_data = super(ItemForm, self).clean()
        if len(cleaned_data["context"]) == 0:
            cleaned_data["context"] = [context.id for context in models.Context.objects.all()]
        return cleaned_data


class ItemCommentForm(forms.ModelForm):
    class Meta:
        model = models.ItemComment
        fields = ("memo", )


class JoinForm(UserCreationForm):
    name = forms.CharField(label=_("Name"), max_length=30)
    username = forms.RegexField(label=_("Account"), max_length=30,
        regex=r'^[\w.@+-]+$',
        help_text=_("Letters, digits and @/./+/-/_ only."),
        error_messages={
            'invalid': _("This value may contain only letters, numbers and "
                         "@/./+/-/_ characters.")})
    password1 = forms.CharField(label=_("Password"),
        widget=forms.PasswordInput)
    password2 = forms.CharField(label=_("Password confirmation"),
        widget=forms.PasswordInput,
        help_text=_("Enter the same password as above."))

    email = forms.EmailField(widget=forms.TextInput(attrs=dict({'class': 'required'},
                                                               maxlength=75)),
                             label=_("E-mail"))
    error_messages = {
        'duplicate_username': _("A user with that username already exists."),
        'password_mismatch': _("The two password fields didn't match."),
    }

    def clean_email(self):
        """
        Validate that the supplied email address is unique for the
        site.

        """
        if User.objects.filter(email__iexact=self.cleaned_data['email']):
            raise forms.ValidationError(_("This email address is already in use. Please supply a different email address."))
        return self.cleaned_data['email']

    def clean_username(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"]
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'])
        return password2

    def save(self, commit=True):
        user = super(JoinForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
            models.Profile(user=user, name=self.cleaned_data["name"]).save()

        return user

    class Meta:
        fields = ("name", "email", "username",)
        model = User
