import json
from django.http import HttpResponse
from django.views.decorators.http import require_http_methods as _require_http_methods
from django.contrib import admin


def render_json(method):
    def decorated(*args, **kwargs):
        return HttpResponse(json.dumps(method(*args, **kwargs)),
                            mimetype="application/json")
    return decorated


def require_http_methods(*args):
    return _require_http_methods(args)


def register_admin(cls):
    admin.site.register(cls, admin.ModelAdmin)
    return cls
