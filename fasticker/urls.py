import settings
from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'fasticker.views.home', name='home'),
    # url(r'^fasticker/', include('fasticker.foo.urls')),
    url(r'^$', 'fasticker.core.views.index'),
    url(r'^items$', 'fasticker.core.views.items'),
    url(r'^recommendations$', 'fasticker.core.views.recommendations'),
    url(r'^board/(\d+)$', 'fasticker.core.views.board'),
    url(r'^item/(\d+)$', 'fasticker.core.views.item'),
    url(r'^item/(\d+)/edit$', 'fasticker.core.views.item_form'),
    url(r'^item/(\d+)/comment$', 'fasticker.core.views.item_comment_form'),
    url(r'^item/add$', 'fasticker.core.views.item_form'),
    url(r'^join$', 'fasticker.core.views.join'),
    url(r'^profile/(\d+)$', 'fasticker.core.views.profile'),

    url(r'^login$', 'django.contrib.auth.views.login',
        {"extra_context": {'next': '/'}}),
    url(r'^logout$', 'django.contrib.auth.views.logout',
        {'next_page': '/'}),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
   )
