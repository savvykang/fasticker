import collections
import itertools
import random
import string
from PIL import Image, ExifTags, ImageFile


def random_str(size=32):
    letters = list(itertools.chain(string.digits, string.lowercase))
    return ''.join(random.choice(letters) for _ in xrange(size))


def flatten(l):
    for el in l:
        if isinstance(el, collections.Iterable) and not isinstance(el, basestring):
            for sub in itertools.islice(flatten(el)):
                yield sub
        else:
            yield el


def unique_everseen(iterable, key=None):
    "List unique elements, preserving order. Remember all elements ever seen."
    # unique_everseen('AAAABBBCCDAABBB') --> A B C D
    # unique_everseen('ABBCcAD', str.lower) --> A B C D
    seen = set()
    seen_add = seen.add
    if key is None:
        for element in itertools.ifilterfalse(seen.__contains__, iterable):
            seen_add(element)
            yield element
    else:
        for element in iterable:
            k = key(element)
            if k not in seen:
                seen_add(k)
                yield element


def autorotate(filename):
    image = Image.open(filename)
    should_save = False
    for orientation in ExifTags.TAGS.keys():
        if ExifTags.TAGS[orientation] == 'Orientation':
            break
    if hasattr(image, '_getexif'):  # only present in JPEGs
        e = image._getexif()        # returns None if no EXIF data
        if e is not None:
            exif = dict(e.items())
            orientation = exif[orientation]
            orient_to_option = {3: Image.ROTATE_180,
                                6: Image.ROTATE_270,
                                8: Image.ROTATE_90}
            option = orient_to_option.get(orientation, None)
            if option:
                image = image.transpose(option)
                should_save = True

    if any(dim > 800 for dim in image.size):
        image.thumbnail((800, 800), Image.ANTIALIAS)
        should_save = True

    if should_save:
        try:
            image.save(filename, quality=90, optimize=True)
        except IOError:
            ImageFile.MAXBLOCK = image.size[0] * image.size[1]
            image.save(filename, quality=90, optimize=True)


def media_upload_to(instance, filename):
    ext = filename.split('.')[-1]
    filename = u'{}.{}'.format(random_str(32), ext)
    return u'/'.join([type(instance).__name__.lower(),
                     filename])
