import requests
import urlparse
import json
import time
from bs4 import BeautifulSoup

item_index = 0
set_index = 0
contains_index = 0

def page_item(url):
    def dump(title="", image=""):
        global item_index
        item_index += 1
        return dict(model="core.item", pk=item_index+1,
                    fields=dict(name=title,
                                image_url=image,
                                memo="none",
                                tags=[1]))
    resp = requests.get(url)
    assert resp.ok
    soup = BeautifulSoup(resp.content)
    title = soup.h1
    img = soup.find(attrs={"id":"thing_img"}).img
    yield dump(title=title.text, image=img["src"])

def page_set(url):
    def dump(title="", image=""):
        global set_index
        set_index += 1
        return dict(model="core.set", pk=set_index+1,
                    fields=dict(name=title,
                                image_url=image,
                                author=1))
    def dump_relation(set="", item=""):
        global contains_index
        contains_index += 1
        return dict(model="core.containsrelation", pk=contains_index+1,
                    fields=dict(set=set,
                                item=item))

    resp = requests.get(url)
    assert resp.ok
    soup = BeautifulSoup(resp.content)
    title = soup.h1
    image = soup.find('img', attrs={"class": "main_img"})

    set_obj = dump(title=title.text, image=image["src"])
    yield set_obj

    for li in soup.find(attrs={"class":"mod_inline_save"}).find_all('li'):
        title = li.find(attrs={"class": "name"})
        for x in page_item(urlparse.urljoin(url, title.a["href"])):
            yield x
            yield dump_relation(set_obj["pk"], x["pk"])
            time.sleep(0.25)


def page(url):
    resp = requests.get(url)
    assert resp.ok
    soup = BeautifulSoup(resp.content)
    for li in soup.find(attrs={"class":"layout_n"}).find_all('li'):
        img = li.img.get("xsrc") if li.img else None
        title = li.h2
        if img and title and "set" in title.a["href"]:
            for x in page_set(urlparse.urljoin(url, title.a["href"])):
                yield x
                time.sleep(0.25)

l = []
try:
    for x in page('http://www.polyvore.com/celebrity/splash.topic?topic=Celebrity&page=2'):
        l.append(x)
except:
    print json.dumps(l)
